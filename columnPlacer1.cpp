#include<iostream>
#include <iomanip>
#include<math.h>
#include<fstream>
using namespace std;

int main()
{
int i=1;

/* radius of water tank base */
int radius = 5; 

/* initial angle in degrees */
int angleDeg = 0;

/* increment in angle in degrees */
int  angleStepDeg = 45;

/* x coordinate, y coordinate, initial angle in radian, increment in angle in radian, maximum value of angle */
double xCord, yCord, angleRad, angleStepRad, maxRange;

/* converting angle from degrees into radians */
angleRad = angleDeg * (M_PI / 180);

/* converting increment angle from degrees to radians */
angleStepRad = angleStepDeg * (M_PI / 180);

/* converting maximum angle from degrees to radians */
maxRange = 360 * (M_PI / 180);

/* creating object of class ofstream, to save data in a file, columnPlacer.sh */
ofstream oFile;
oFile.open("columnPlacer.sh");
oFile<<"#!/bin/bash\n";

/* while initial angle is less than maximum angle */
while (angleRad < maxRange)
{

/* using formulae, "x = r Sin (theta)" and "y = r Cos(theta)", 
   calculating the value of x and y coordinates of vertices of RCCs */
xCord = radius * sin(angleRad);
yCord = radius * cos(angleRad);

/* incementing the angle */
angleRad = angleRad + angleStepRad;

/* setting the output of floating numbers from scientific to normal form */
cout.setf(ios::fixed);
oFile.setf(ios::fixed);

//cout<<setprecision(3)<<xCord<<endl;
//cout<<setprecision(3)<<yCord<<endl;

/* saving the mged command in a file */
oFile<<"mged -c waterTank1.g in col"<<i<<" rcc "<<setprecision(3)<<xCord;
oFile<<" "<<setprecision(3)<<yCord<<" 24 0 0 10 0.5\n";
i++;
}

/* closing the file */
oFile.close();
return(0);
}
