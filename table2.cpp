/*
This program accepts input from input file, writes into the shell script which 
will be executed from this program using system().
*/

#include<iostream>
#include<fstream>	// header file for file handling
#include<string.h>
#include<stdlib.h>	// class of system()

using namespace std;

/* Class to create box shaped objects. */
class box
{
	/* command variables */
	string databaseName, shapeName, xmin, xmax, ymin, ymax, zmin, zmax;
	
	 /* buffer to store shell command */
	string cmd[19];
	
	public:
	/* function to get input from user */
	void receiveData(string &, string &, string &, string &, string &, string &, string &, string &);
	void processData();

};

void box :: receiveData(string &databaseName, string &shapeName, string &xmin, string &xmax, string &ymin, string &ymax, string &zmin, string &zmax)
{	
	/* creating file object for accepting input from user */
  	ifstream iFile;
        iFile.open("inputFile.txt");
        iFile>>databaseName;
        iFile>>shapeName;
        iFile>>xmin;
        iFile>>xmax;
        iFile>>ymin;
        iFile>>ymax;
        iFile>>zmin;
        iFile>>zmax;
        iFile.close();
}

void box :: processData()
{
	receiveData(databaseName, shapeName, xmin, xmax, ymin, ymax, zmin, zmax);	
	
	cmd[0] = "mged -c ";
	cmd[1] = databaseName;
	cmd[2] = " ";
	cmd[3] = "in";
	cmd[4] = " ";
	cmd[5] = shapeName;
	cmd[6] = " ";
	cmd[7] = "rpp ";
	cmd[8] = xmin;
	cmd[9] = " ";
	cmd[10] = xmax;
	cmd[11] = " ";
	cmd[12] = ymin;
	cmd[13] = " ";
	cmd[14] = ymax;
	cmd[15] = " ";
	cmd[16] = zmin;
	cmd[17] = " ";
	cmd[18] = zmax;
	
	/* creating file object for sending output to file */
	ofstream oFile;
	oFile.open("script.sh");
	for (int i = 0; i < 20; i++)
	{
	oFile << cmd[i];
	}
	oFile.close();

	/* executing shell command(s) */
	system("ls");
}


int main()
{	
	string databaseNa, shapeNa, xmin, xmax, ymin, ymax, zmin, zmax;
	
	/* Create object "tableSurface" of class "box" */
	box tableSurface;

	tableSurface.receiveData(databaseNa, shapeNa, xmin, xmax, ymin, ymax, zmin, zmax);
	tableSurface.processData();	

return(0);
}
