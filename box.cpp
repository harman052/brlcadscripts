/*
This program accepts input from input file, writes into the shell script which 
will be executed from this program using system().
*/

#include<iostream>
#include<fstream>	// header file for file handling
#include<string.h>
#include<stdlib.h>	// class of system()

using namespace std;

/* Class to create box shaped objects. */
class box
{
	int i;
	 /* buffer to store shell command */
	string cmd[24];
	
	public:
	/* function to get input from user */
	box();

};

box :: box()
{	
i=0;
ifstream iFile1, iFile2;
//iFile1.open("constant");
iFile2.open("var");

while( i < 23 )
{
if (i == 0)
{
//iFile1 >> cmd[i];
}
else if(i%2 != 0)
{
cmd[i]=" ";
}
else
{
iFile2 >> cmd[i];
}
cout<<cmd[i];
i++;
}
cout<<endl;
cmd[i]="\0";

ofstream oFile;
oFile.open("script");
oFile<<"#!/bin/bash\n";
oFile<<"rm -f *.g\n";
oFile<<"rm -f rt*\n";
oFile<<"rm -f ../../cgi-images/rt*\n";
oFile<<"rt=rtFile\n";
oFile<<"cat <<EOF | env /usr/brlcad/bin/mged -c "<<cmd[2];
oFile<<"\nin ";
int j;
for (j=4; j<i; j++)
{
oFile << cmd[j];
}     
//cout<<endl;
oFile<<"\nr table.r u "<<cmd[4];
oFile<<"\nmater table.r plastic 12 164 220 0\n";
oFile<<"B table.r\n";
oFile<<"ae 25 35\n";
oFile<<"saveview $rt\n";
oFile<<"EOF\n";
oFile<<"sed '2cenv /usr/brlcad/bin/rt -M \\\\' $rt > tempFile\n";
oFile<<"rm $rt\n";
oFile<<"mv tempFile $rt\n";
oFile<<"chmod 755 $rt\n";
oFile<<"./$rt\n";
oFile<<"env /usr/brlcad/bin/pix-png < $rt.pix > $rt.png\n";
oFile<<"cp $rt.png ../../cgi-images\n";
//iFile1.close();
iFile2.close();
oFile.close();
//system("bash script");
}
int main()
{	
	/* Create object "tableSurface" of class "box" */
	box tableSurface;

return(0);
}
