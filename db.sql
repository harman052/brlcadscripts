-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 24, 2012 at 04:52 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `harman`
--

-- --------------------------------------------------------

--
-- Table structure for table `commonAttributes`
--

CREATE TABLE IF NOT EXISTS `commonAttributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute` varchar(20) NOT NULL,
  `defaultValue` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `commonAttributes`
--

INSERT INTO `commonAttributes` (`id`, `attribute`, `defaultValue`) VALUES
(1, 'Database name', 'testdb.g'),
(2, 'Shape or object name', 'object1.s'),
(3, 'Shape type', 'rpp');

-- --------------------------------------------------------

--
-- Table structure for table `shapeAttributes`
--

CREATE TABLE IF NOT EXISTS `shapeAttributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shape_id` int(11) NOT NULL,
  `attribute` varchar(20) DEFAULT NULL,
  `default_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shape_id` (`shape_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `shapeAttributes`
--

INSERT INTO `shapeAttributes` (`id`, `shape_id`, `attribute`, `default_value`) VALUES
(1, 1, 'xmin', 2),
(2, 1, 'xmax', 8),
(3, 1, 'ymin', 4),
(4, 1, 'ymax', 8),
(5, 1, 'zmin', 5),
(6, 1, 'zmax', 11),
(7, 2, 'x coordinate of cent', 4),
(8, 2, 'y coordinate of cent', 4),
(9, 2, 'z coordinate of cent', 4),
(10, 2, 'radius', 3);

-- --------------------------------------------------------

--
-- Table structure for table `shapeType`
--

CREATE TABLE IF NOT EXISTS `shapeType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shapeType` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `shapeType`
--

INSERT INTO `shapeType` (`id`, `shapeType`) VALUES
(1, 'rpp'),
(2, 'sph'),
(3, 'rcc');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `shapeAttributes`
--
ALTER TABLE `shapeAttributes`
  ADD CONSTRAINT `shapeAttributes_ibfk_1` FOREIGN KEY (`shape_id`) REFERENCES `shapeType` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
